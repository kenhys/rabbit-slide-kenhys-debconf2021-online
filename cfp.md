Latest updates about fabre.debian.net - Finding untouched bugs

#### Introduction

At DebConf20, I gave a short talk about an idea to improve
user interfaces to find untouched bugs and much more.

Some idea had been implemented and also deployed as an experimental service on fabre.debian.net.

#### Target audience

* Debian contributor or maintainer who want to fix bugs not only the bugs which
is related to maintained packages by you

It may not interesting for Debian developer because DD is already familiar with bugs.debian.org, udd.debian.org, or qa.debian.org.

#### What will talk about

In this session, I’ll talk latest updates about fabre.debian.net since DebConf20.

#### References

* [Experiment about Debian's bug tracking front-end](https://debconf20.debconf.org/talks/28-experiment-about-debians-bug-tracking-front-end/) DebConf20
    * slide: [An experiment about personalized front-end of bugs.debian.org](https://slide.rabbit-shocker.org/authors/kenhys/debconf2020-online/)
* [fabre.debian.net - Finding untouched bugs](https://in2021.mini.debconf.org/talks/56-lightning-talks/) MiniDebConf India 2021
    * slide: [An experiment about personalized front-end of bugs.debian.org](https://slide.rabbit-shocker.org/authors/kenhys/minidebconf-india2021/)
* Ubuntu Streaming Meeting 21.01 (Lightning talk)
    * slide: https://slide.rabbit-shocker.org/authors/kenhys/ubuntu-streaming-meeting-2101/ (Written by Japanese)
