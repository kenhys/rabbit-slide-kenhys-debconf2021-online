# DebConf21 fabre.debian.netの最近のトピック紹介

## 目的

DebConf20以降の最近のトピックを紹介する。
Debian.net teamによるスポンサーされているので有用性を示す。

## 最近のトピック

* DebConf20以降の最新のトピックを紹介し、継続的に改善していることを示す。
* 実際に使われていること・使えることを示して、有用性を示す

### DebConf20で実現できていたこと

* RC,Updated,Createdなバグの表示
* blockedのリスト表示
* controlへのmailto: リンク表示
* bookmark, affected, installedを表示するDashboard
* 30日以内にコメントたバグへのリンク

### DebConf20以降で実現したこと

* よりDebianっぽいデザインに変更した
* fabre.debian.netのサブドメインを取得した
* スポンサーを得た(FOSSHOST)
* スポンサーを得た(Debian.net team)
* ページングに対応した
* コントロールメール送信方法を増やした(以前はcontrolのみだった)
* FTBFSやSecurityカテゴリを表示するようにした
* カテゴリごとでseverityごとにわけて表示できるようにした
* コメントがまだついていないもののみを表示できるようにした(Fixme)
* ArchitectureTagsの表示に対応した

### トピックすべてを解説するのではなく便利なシナリオを紹介する

* まだだれも手をつけていないバグをなおしたい
  * Fixmeを参照すればいい
* RCバグの修正を手伝いたい(主要パッケージではなくてもよい)
  * まだ誰も手をつけていないやつにとりかかる Fixmeから探す
* FTBFSなやつをなおしたい(new!)
  * GCCを更新したときに起きるやつ FTBFSカテゴリを参照する
* 特定アーキテクチャでのみ発生する問題をなおしたい(new!)
  * ArchitectureTagsカテゴリを参照するとよい
* 移行がすすんでいないバグを修正したい(new!)
  * blocked byが多いトラッキングのチケットを探す TODO:これまだバグありそう

# 今後の予定

* TODO: 自由に全文検索できる
* TODO: パーソナライズされた検索ができる
