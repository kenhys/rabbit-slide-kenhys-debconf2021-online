# Latest topics about fabre.debian.net 

subtitle
:  Finding untouched bugs 🐞

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  DebConf21 online August 28, 2021

allotted-time
:   15m

theme
:  .

# See Rabbit Slide Show

![https://slide.rabbit-shocker.org/authors/kenhys/debconf2021-online/](images/debconf2021-presentation.png){:relative-height="100"}

# Personal profile

![](images/profile.png){:relative-height="40"}

* Debian Developer (@kenhys) since Sep, 2020
* Trackpoint(soft dome) and Wasa beef(Yamayoshi Wasabi Potato Chips) fan
* Working for ClearCode Inc.

# ClearCode Inc.

![https://www.clear-code.com/](images/logo-combination-standard.svg){:relative-height="35"}

* It is important to do both free software activities and develop/support software business
* We feedback our business experiences to free software

# Agenda

* What is Fabre?
* Recall DebConf20 presentation
* What I want to solve
* Useful use cases
* Summary

# What is Fabre?

![https://fabre.debian.net/](images/fabre.debian.net.png){:relative-height="80"}

* Small web service to find untouched bugs

# Recall DebConf20 presentation summary

![https://slide.rabbit-shocker.org/authors/kenhys/debconf2020-online/](images/debconf2020-presentation.png){:relative-height="100"}

# What I want to solve

* The troublesome cases about tracking bugs
  * Mainly package or maintainer oriented approach
    * <https://qa.debian.org/>
    * <https://tracker.debian.org/>
    * <https://udd.debian.org/dmd/>

* Not so easy cases 🤔
  * Find only untouched bugs
  * See blocked bugs at a glance 👀
  
# How to solve this situation?

* Provide simple front-end as an experimental service!
  * Mashup existing data
    * Import E-mail archives (debian-bugs-dist,debian-bugs-closed)  📧 
    * Use UDD metadata (udd-mirror@udd-mirror.debian.net) 💽

# fabre.debian.net

![](images/fabre-recent-updated.png){:relative-height="100"}

# Latest topics

* Fabre had been deployed under debian.net
* Sponsorship for fabre.debian.net
* And some more minor improvements

# Fabre had been deployed under debian.net

* At first, Fabre was deployed under my own domain
* fabre.debian.net was owned by other DD (unused for a long time)
* Negotiated about fabre.debian.net subdomain with DSA (October, 2020)
* Got fabre.debian.net subdomain and deployed (November, 2020)

# Sponsorship for fabre.debian.net

* May 2020 - Oct, 2020 (No sponsor, hosted on WebARENA/Indigo)
  * There is the issue about limited server resources
* Oct, 2020 - June, 2021 (Sponsored by FOSSHOST)
* June, 2021 - (Sponsored by Debian.net Team)
  * The issue about server resource has been solved! 🎉

# Off-Topic: Request hosting sponsor

![https://salsa.debian.org/debian.net-team/requests/-/issues](images/debian.net-team-requests.png){:relative-height="100"}

# And some more minor improvements

* Improved viewing bugs by category, severity
* Added button to share bug report (Tweet)
* Added more mailto: button to control bug report
* Added to package description in each bug report
* ...

# Use cases about fabre.debian.net

* Finding a bug that no one working on
* Sending control E-mail easily
* Easy to view blocked bugs
* Finding a FTBFS bug 🆕
* Finding a security bug 🆕
* Finding a architecture specific bug 🆕

# Finding a bug that no one working on

![](images/easy-to-find-no-one-working-mark.png){:relative-height="80"}

* Show the number of comments for your hints

# Sending control E-mail easily

![](images/easy-to-send-control-email-mark.png){:relative-height="70"}

* Show mailto: buttons to control bugs on each bug report
  * Tagging Architecture is supported

# Easy to view blocked bugs

![](images/easy-to-view-blocking-mark.png){:relative-height="90"}

* List blocked bugs appropriately

# Finding a FTBFS bug 🆕

![https://fabre.debian.net/bugs/ftbfs/fixme](images/easy-to-find-ftbfs.png){:relative-height="100"}

# Finding a security bug 🆕

![https://fabre.debian.net/bugs/security/fixme](images/easy-to-find-security.png){:relative-height="100"}

# Finding a architecture specific bug 🆕

![https://fabre.debian.net/bugs/archtag/arm64](images/easy-to-find-archtag.png){:relative-height="100"}

# Summary

* Fabre had been deployed as debian.net subdomain
* Fabre had been sponsored by Debian.net team
* I've implemented what I want on fabre.debian.net since DebConf20
  * New FTBFS,Security,ArchTag categories and so on

# Any questions?

* Could you speak slowly if you have a question, please?
* Use <https://pad.online.debconf.org/p/16-latest-updates-about-fabredebiannet-finding-u> for asking a question
  * I'll follow-up after this presentation a bit 😪
