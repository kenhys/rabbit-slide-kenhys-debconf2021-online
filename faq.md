Q. プレゼンテーションツールには何を使っているのか?
   What are you using presentation tool?
A. I'm using rabbit as a presentation tool.
   You can write manuscript in markdown.
   See rabbit-shocker.org in details.
   https://github.com/rabbit-shocker/rabbit
   https://rabbit-shocker.org/en/

   You can also publish slide as a Ruby gems. (presentation source code is available!)
   https://slide.rabbit-shocker.org/authors/kenhys/debconf2021-online/
  
Q. ソースコードは公開されていますか? Is Fabre source code is accessible?
A. It is hosted on salsa, licensed under LGPL, 
   but it is not ready to collaborate with others, no documentation,
   so setup your own instance is not realistic.
   Sure, I'll improve current situation, but the priority is not so high.

Q. どれくらいの頻度でバグが更新されていますか?
A. There are some kind of tasks to import new buggs.
   Every 15 minutes. bug E-mail report was imported.
   Every day, archived bugs are imported. This means that archived bugs will be hidden.
   Importing metadata from udd-mirror is done in every 3 hours. (affected release? unstable/testing/stable)
   Thus, almost new bugs are imported in 3 hours.

Q. 以前バグをブックマークする機能があったけどどうなった?
A. Personalized feature is still in beta.
   Currently, it is not public yet.
   I'm working on refactoring code base especially this feature.
   I'll publish it in the future again.

Q. どれくらいのリソースが必要ですか? How much resource is required?
A. If you want launch Fabre by your own, at least 2 GB memory or more is recommended.
   Currently fabre.debian.net is hosted on Hetzner CPX21 instance. (4GB memory and 3 core)

Q. Fabreでメールを送れるようにしませんか? Not only mailto: button, do you have a plan to send email directly from fabre.d.n?

  I thought that sending E-mail automatically is not appropriate
  but, it is reasonable to provide such a feature for salsa user account.
  
Q. Salsaユーザーアカウントでログインできますか? Can I log in with salsa account?

   Yes, but such feature is disabled by default. it is because of refactoring of changing internal code base.
  
Q. なんでGroongaを使っているのですか? Why did you use Groonga?
A. Because I maintain Groonga package and I also want to support fulltext-search with it.
   Groonga is a column store database which supports fulltext-search.

Q. How spec is changed from FOSSHOST

* My private VPS era, fabre.xdump.org 512MB, 1vCPU
* On FOSSHOST, fabre.debian.net 2GB memory, 4vCPU
* On Debian.net team, fabre.debian.net 4GB memory, 3vCPU

