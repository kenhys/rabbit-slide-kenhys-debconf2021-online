# Short talk manuscript

Hi, thank you for coming to this short talk.

I'll start the presentation "Lastest topics about fabre.debian.net".

## See Rabbit Slide Show

This slide was already published at Rabbit Slide Show

So, you can see this presentation later.

## Personal profile

Let me just start by introducing myself.

I'm one of a Debian Developer since last year.
I'm working for ClearCode.

## ClearCode Inc.

Usually, I'm developing and supporting software as an engineer.

There is a culture to respect free software activity,
 so I could prepare this presentation at work.

# Agenda

Here is the short talk agenda.

Maybe you don't know Fabre, I'll explain it first.
It is appropriate to show last year's presentation to make you know Fabre well.

Next, I'll explain the issues what I want to solve and 
useful use cases by Fabre.

# What is Fabre?

Here is the screenshot of Fabre.

In short, It's a small web service to find untouched bugs.

# Recall DebConf20 presentation summary

In the last year's DebConf, I gave you a short talk about Fabre.

Fabre is an experiment to make a better bug tracking experience,
and the way to achieve such a purpose.

In this slide, I've explained the current problems and
the idea how to solve it, system internals about Fabre web service.

# What I want to solve

There are many services in Debian project.
qa, tracker, debian maintainer dashboard and so on.

But in point view of tracking bugs, there is a case that it is hard to do it.
This is because these services are aimed to help package maintainer.

For example, 
It is troublesome if you find only untouched bugs.
or see blocked bugs at a glance.
It seems that these issues are not covered well.

# How to solve this situation?

Then I've started an experimental service to solve these problems.

To achieve it, I've used to mashup existing data.
E-mail archive is used to control syncing intervals.
UDD metadata is used to track status of bugs.

# fabre.debian.net

Here is the screenshot of fabre.debian.net

In the left side, most commonly used categories are put on.

In the right side, list of bugs are shown.

# Latest topics

Here is the latest topics of Fabre.

First topic is domain of Fabre.
The second one is about sponsorship.
The last one is minor improvements.

# Fabre had been deployed under debian.net

At first, Fabre was not deployed on debian.net.
Since I've become a Debian Developer, I've contacted DSA and
finally transferred subdomain ownership to me.
As a result, I could manage fabre.debian.net.

# Sponsorship for fabre.debian.net

Next topic is sponsorship

Fabre didn't have sponsor at first.
It was deployed on very small VPS instance on WebARENA, so there was a issue of limited server resources.
It causes out of memory frequently, sometimes it must be restarted.

But I could got sponsor - It was FOSSHOST.
As you know, there was a concern about continuity before,
I've stopped to use it and got new sponsor - Debian.net team infrastructure.

As a result, the issue of server resources are solved completely.

# Off-Topic: Request hosting sponsor

It's a off topic, 
You can request hosting sponsorship to Debian.net team on salsa.

# And some more minor improvements

The last topic is some more improvements since last year,
but it is a too minor ones, so I'll show you useful use cases instead.

# Use cases about fabre.debian.net

There are 6 use cases to show you.

Finding a bug that no one working on
Sending control E-mail easily
Easy to view blocked bugs
Finding a FTBFS bug
Finding a security bug
Finding a architecture specific bug

# 1st: Finding a bug that no one working on

It shows the number of comments for your hints in list of bugs.
And Fixme tab is applied to filter bugs in this slide.

# 2nd: Sending control E-mail easily

In every bug report page, it contains mailto: buttons to control bugs.
If you click these buttons, it launches your E-mail client.

In this case, you can send control E-mail for tagging architecture as a bug report.

# 3rd: Easy to view blocked bugs

In contrast to bugs.debian.org, it shows blocked by bugs at a glance.
If there are plenty of blocked by bugs, this feature is very useful.

# 4th Finding a Fail to build from source bug

It is a new feature, I've added fail to build from source category.
There are over 200 untouched bugs.
Most of these bugs are tagged as serious or important.

# 5th Finding a security bug

It is also a new feature, I've added security category, too.
There are over 300 untouched bugs.
Most of these bugs are tagged as important or normal.

# 6th Finding a architecture specific bug

It is also a new feature, I've added architecture category, too.
Architecture tag was formalized recently, at most seventy bugs are registered now.

# Summary

As a summary,

Fabre had been deployed as debian.net and sponsored by Debian.net team infrastructure.
Fabre is continuously updated since last year, and I'll implement what I want to do in the future.

# Any questions?

And that's it, thank you.
Could you speak slowly when you ask, please?
