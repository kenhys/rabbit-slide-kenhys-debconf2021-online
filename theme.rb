@default_font = "Ubuntu"
@monospace_font = "Ubuntu"
include_theme("debian")
@slide_show_span = 1800
@slide_show_loop = true

# Tweak debian theme for 1280x720
#
# match(Slide, HeadLine) do |heads|
#  loader_head.resize(canvas.width, 190)
# end
